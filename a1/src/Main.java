import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashMap<String, Integer> gamesMap = new HashMap<>();
        gamesMap.put("Mario Odyssey", 50);
        gamesMap.put("Super Smash Bros. Ultimate", 20);
        gamesMap.put("Luigi's Mansion 3", 15);
        gamesMap.put("Pokemon Sword", 30);
        gamesMap.put("Pokemon Shield", 100);
//        System.out.println(gamesMap);

        ArrayList<String> topGames = new ArrayList<>();

        gamesMap.forEach((key,value)->{
            System.out.println(key + " has "+value+" stocks left.");
        });

        gamesMap.forEach((key,value)->{
            if(value<=30){
                System.out.println(key+" has been added to top games list!");
                topGames.add(key);
            }
        });

        System.out.println("Our shop's top games:");
        System.out.println(topGames);
    }
}