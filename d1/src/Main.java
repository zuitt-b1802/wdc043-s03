import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Loops are control structures that allow code blocks to be repeated according to considtions set.
        //Types of loops
        // 1. For Loop
        // 2. While Loop
        // 3. Do-While Loop

        //While loop
        int a = 1;

        while(a<5){
            System.out.println("While loop counter: "+a);
            a++;
        }

        int b = 12;
        do{
            System.out.println("Countdown: "+b);
            b--;
        }while(b>10);

        //While Loop with User Input
        Scanner appScanner = new Scanner(System.in);
        String name = " ";

        //isBlank() is a string method, which checks the non-whitespace characters in our string and will return true ven if there is only a blank/whitespace.®
        System.out.println(name.isEmpty());// will return false if there is at least whitespace in the string
        System.out.println(name.isBlank());
        while(name.isBlank()){
            System.out.println("What is your name?");
            name = appScanner.nextLine();

            if(!name.isBlank()){
                System.out.println("Hi! "+name);
            }
        }
        // For loops - are more versatile than our while loops
        for(int i = 1;i<=10;i++){
            System.out.println("Count: " +i);
        }

        //For loop over a Java array
        int[] intArray = {100,200,300,400,500};
        for (int i=0;i<intArray.length;i++){
            System.out.println("Item Index:"+intArray[i]);
        }

        //Loop over Multidimensional Array
        //Multidimensional Arrays
        // A two-dimensional array, which can be best be described by two lengths nested within each other like a matrix.
        //The first array could be for rows, the second could be for cols
        String[][] classroom = new String[3][3];

        //first row
        classroom[0][0] = "Rayquaza";
        classroom[0][1] = "Kyogre";
        classroom[0][2] = "Groudon";

        //second row
        classroom[1][0] = "Sara";
        classroom[1][1] = "Goofy";
        classroom[1][2] = "Donald";

        //third row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermoine";

        System.out.println(Arrays.toString(classroom));

        //Nested For Loop
//        for(int row = 0; row<3;row++){
//            for(int col = 0; col<3;col++){
//                System.out.println(classroom[row][col]);
//            }
//        }

        //Enhanced for loop for Java Array/ArrayList
        //In Java, a for-each loop can be used to iterate over the items of an array and arrayList
        //for-each in hava for array and arrayList is also called enhanced for loop

        String[] members = {"Eugene","Vincent","Dennis","Alfred"};

        //member is a parameter which will represent each item in the given array
        for(String member: members){
            System.out.println(member);
        }

        //Multidimensional Array for each
        //outer where String[] as outer is declared as bracket since it is a row of arrays
        for(String[] row:classroom){
            //inner, we had accessed the row and now be accessing the elements on that row of array
            for(String student:row){
                System.out.println(student);
            }
        }

        //HashMap forEach
        //HashMap has a method for iterating each field-value pair.
        //The HashMap forEach() requires a lambda expression as arguement
        //A lambda expression in Java, is a short block of code which takes in parameters and returns a value. Lambda expressions a re similar to methods, but they do not need a name and they can be implemented within another method.

        HashMap<String, String> techniques = new HashMap<>();
        techniques.put(members[0], "Spirit Gun");
        techniques.put(members[1], "Black Dragon");
        techniques.put(members[2], "Rose Whip");
        techniques.put(members[3], "Spirit Sword");
        System.out.println(techniques);

        techniques.forEach((key,value)->{
            System.out.println("Member "+ key + " uses "+value);
        });

        //Exception Handling (Scanner)
        //As the Scanner methods have specific dataTypes associated with them (nextLine(), nextInt(), nextDouble()), it is best to assume that there will be issues in user input especially if they fail to provide the correct data type.
        //Exception Handling refers to managing and catching run-time errors in order to safely run our code
        System.out.println("Enter an integer:");


        try{
            int num = appScanner.nextInt();
        }catch(Exception e){
            System.out.println("Invalid Input");
            e.printStackTrace();
        }
    }
}